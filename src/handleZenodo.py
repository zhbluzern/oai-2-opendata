import json
from datetime import datetime
import glob
import os

def createZenodoMetadata(row):
    with open('ZenodoData.json', 'r') as dataFile:
        InvenioData=dataFile.read()
    recordSchema = json.loads(InvenioData)
    metadata = recordSchema
    print(metadata)
    metadata["metadata"]["title"] = row.title
    metadata["metadata"]["publication_date"] = datetime.today().strftime('%Y-%m-%d')  
    #creator = zen.setPersonOrOrg(row["dataOwner"], type="organizational", persIdScheme="gnd", persId=row["dataOwner_GND"], role="DataCollector")
    creator = {"name":row.dataOwner, "gnd":row.dataOwner_GND}
    metadata["metadata"]["creators"].append(creator)
    #creator = zen.setPersonOrOrg("ZentralGut",type="organizational", role="HostingInstitution")
    creator = {"name":"ZentralGut"}
    metadata["metadata"]["creators"].append(creator)
    metadata["metadata"].update({"description": row.description})
    relId = {"identifier":row.ZentralGutUrl,"relation":"isMetadataFor"}
    metadata["metadata"]["related_identifiers"].append(relId)

    metadata["metadata"]["communities"].append({"identifier":"lara_zentralgut_datasets"})   
    
    return metadata

def uploadFiles(zenodo, row):
    fileNames=glob.glob(f"Files/{row['outputFileName']}*")
    uploadFiles = [os.path.basename(file) for file in fileNames]
    for file in uploadFiles:
        zenodo.uploadFile(file, filePath="Files/")

if __name__ == "__main__":
    md = createZenodoMetadata(row="")
    print(md)