import src.ZenodoSearch as ZenodoSearch
import glob
import os
from datetime import datetime

def prepareDraft(zen, row):
    ### Prepare the necessary Metadata for the Zenodo Record
    zen.recordSchema["metadata"]["publication_date"] = datetime.today().strftime('%Y-%m-%d')
    creator = zen.setPersonOrOrg(row["dataOwner"], type="organizational", persIdScheme="gnd", persId=row["dataOwner_GND"], role="DataCollector")
    zen.recordSchema["metadata"]["creators"].append(creator)
    creator = zen.setPersonOrOrg("ZentralGut",type="organizational", role="HostingInstitution")
    zen.recordSchema["metadata"]["creators"].append(creator)
    zen.recordSchema["metadata"]["title"] = row["title"]
    zen.recordSchema["metadata"]["description"]  = row["description"]
    relId = {"identifier":row["ZentralGutUrl"],"relation_type":{"id":"ismetadatafor"},"scheme":"url"}
    zen.recordSchema["metadata"]["related_identifiers"] = [relId]
    zen.recordSchema["metadata"]["communities"].append({"identifier":"lara_zentralgut_datasets"})   

    # zenSearch = ZenodoSearch.ZenodoSearch()
    # zen.recordSchema["metadata"]["communities"] = []    
    # communityId = zenSearch.getCommunityIdBySlug(slug="lara_zentralgut_datasets")
    # zen.recordSchema["metadata"]["communities"].append({"identifier":communityId})   

    print(zen.recordSchema)
    newRecord = zen.createDraft(zen.recordSchema)
    return newRecord

def uploadFiles(zen, row, newRecord):
    fileNames=glob.glob(f"Files/{row['outputFileName']}*")
    uploadFiles = [os.path.basename(file) for file in fileNames]
    listOfFiles = []
    for file in uploadFiles:
        listOfFiles.append({"key":file})
        #handleFiles.downloadFile(fileUrl[0].text, localPath="nlv/Files")
    fileDraft = zen.startDraftFiles(newRecord["id"], data=listOfFiles)

    for filename in uploadFiles:
        with open(f"Files/{filename}", 'rb') as f:
            data = f.read()
        fileUpload = zen.uploadFileContent(newRecord["id"], fileName=filename, fileContent=data)
        #print(fileUpload)
        print(zen.commitFileUpload(newRecord["id"], fileName=filename))

def acceptAllRequests(zen, newRecord):
    #Search for alle Requests by the new Record-ID
    zenRequ = zen.searchRequests(f"topic.record:{newRecord['id']}")
    results = (zenRequ.json())
    
    for hit in results["hits"]["hits"]:
        print(hit["id"])
        print(hit["type"])
        #print(hit["links"]["actions"]["accept"])
        print(zen.acceptRequest(hit["id"]).text)
