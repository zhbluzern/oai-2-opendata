from lxml import etree
import pandas as pd
from datetime import datetime
import src.ZenodoSearch as ZenodoSearch

class DCAT:
    def __init__(self) -> None:
        
        # Create the root RDF element and namespaces
        self.rdf_ns = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
        self.dcat_ns = "http://www.w3.org/ns/dcat#"
        self.dct_ns = "http://purl.org/dc/terms/"
        self.foaf_ns = "http://xmlns.com/foaf/0.1/"
        self.xml_ns = "http://www.w3.org/XML/1998/namespace"
        self.vcard_ns = "http://www.w3.org/2006/vcard/ns#"

        self.namespaces = {
            None: self.rdf_ns,  # Default namespace
            "dcat": self.dcat_ns,
            "dct": self.dct_ns,
            "foaf": self.foaf_ns,
            "xml" : self.xml_ns,
            "vcard" : self.vcard_ns
        }
        
        self.fileTypes = {
            "text/csv" : "https://publications.europa.eu/resource/authority/file-type/CSV",
            "text/xml" : "https://publications.europa.eu/resource/authority/file-type/XML",
            "application/json" : "https://publications.europa.eu/resource/authority/file-type/JSON"
        }
        self.zenodoSearch = ZenodoSearch.ZenodoSearch()
        self.root = etree.Element("{%s}RDF" % self.rdf_ns, nsmap=self.namespaces)
    
    def getDCATDateTime(self, zenodoDateTime):
        dt = datetime.fromisoformat(zenodoDateTime)
        # Convert to the desired format: YYYY-MM-DDTHH:MM:SSZ
        return dt.strftime("%Y-%m-%dT%H:%M:%SZ")

    def prepareData(self, row):
        dcat_row = row.to_dict()
        files = self.zenodoSearch.getFileInfo(row["ZenodoId"])
        zenodoMd = self.zenodoSearch.getRecordData(row["ZenodoId"])
        dcat_row["files"] = files 
        dcat_row["publication intervall"] = 'http://publications.europa.eu/resource/authority/frequency/AS_NEEDED'
        dcat_row["contactPoints"] = [
            { 'contactPointName' : 'ZentralGut','contactPointMail' : 'zentralgut@zhbluzern.ch'},
            { 'contactPointName' : row["dataOwner"], 'contactPointMail' : row["dataOwner_Mail"]}
            ]
        dcat_row["created"] = zenodoMd["created"]
        dcat_row["modified"] = zenodoMd["modified"]
        dcat_row["publisher"] = "Zentral- und Hochschulbibliothek Luzern (ZHB Luzern)"
        return dcat_row

    def createCatalog(self, catalogUri, catalogTitle,catalogDesc,catalogPublisher):
        self.catalog = etree.SubElement(self.root, "{%s}Catalog" % self.dcat_ns, attrib={"{%s}about" % self.rdf_ns: catalogUri})
        catalog_title = etree.SubElement(self.catalog, "{%s}title" % self.dct_ns)
        catalog_title.text = catalogTitle

        catalog_description = etree.SubElement(self.catalog, "{%s}description" % self.dct_ns)
        catalog_description.text = catalogDesc

        catalog_publisher = etree.SubElement(self.catalog, "{%s}publisher" % self.dct_ns)
        publisher_org = etree.SubElement(catalog_publisher, "{%s}Organization" % self.foaf_ns)
        publisher_name = etree.SubElement(publisher_org, "{%s}name" % self.foaf_ns)
        publisher_name.text = catalogPublisher

        catalog_license = etree.SubElement(self.catalog, "{%s}license" % self.dct_ns,  attrib={"{%s}resource" % self.rdf_ns: "http://creativecommons.org/publicdomain/zero/1.0/"})

        return self.root, self.catalog
    
    def createDataset(self, row):
        dataset = etree.SubElement(self.catalog, "{%s}dataset" % self.dcat_ns)
        dataset_element = etree.SubElement(dataset, "{%s}Dataset" % self.dcat_ns, attrib={"{%s}about" % self.rdf_ns: f"https://sandbox.zenodo.org/record/{row['ZenodoId']}"})

        #Add landing page
        landingPage = etree.SubElement(dataset_element, "{%s}landingPage" % self.dcat_ns)
        landingPage.text = f"https://sandbox.zenodo.org/record/{row['ZenodoId']}"

        # Add title
        title = etree.SubElement(dataset_element, "{%s}title" % self.dct_ns, attrib={"{%s}lang" % self.xml_ns: f"de"})
        title.text = row['title']

        # Add identifier
        identifier = etree.SubElement(dataset_element, "{%s}identifier" % self.dct_ns)
        identifier.text = f"zentralgut.zenodo.doi.{row['ZenodoId']}@zhb-luzern"

        # Add description
        description = etree.SubElement(dataset_element, "{%s}description" % self.dct_ns, attrib={"{%s}lang" % self.xml_ns: f"de"})
        description.text = row['description']

        # Add date information
        issued = etree.SubElement(dataset_element, "{%s}issued" % self.dct_ns, attrib={"{%s}datatype" % self.rdf_ns: "http://www.w3.org/2001/XMLSchema#dateTime"})
        issued.text = self.getDCATDateTime(row['created'])

        modified = etree.SubElement(dataset_element, "{%s}modified" % self.dct_ns, attrib={"{%s}datatype" % self.rdf_ns: "http://www.w3.org/2001/XMLSchema#dateTime"})
        modified.text = self.getDCATDateTime(row['modified'])

        # Add publication interval (accrualPeriodicity)
        accrual_periodicity = etree.SubElement(dataset_element, "{%s}accrualPeriodicity" % self.dct_ns, rdfresource=row['publication intervall'])

        # Add Theme
        #<dcat:theme rdf:resource="https://publications.europa.eu/resource/authority/data-theme/EDUC"/>
        theme = etree.SubElement(dataset_element, "{%s}theme" % self.dcat_ns, attrib={"{%s}resource" % self.rdf_ns: "https://publications.europa.eu/resource/authority/data-theme/EDUC"})

        # Add publisher
        dataset_publisher = etree.SubElement(dataset_element, "{%s}publisher" % self.dct_ns)
        publisher_org = etree.SubElement(dataset_publisher, "{%s}Organization" % self.foaf_ns)
        publisher_name = etree.SubElement(publisher_org, "{%s}name" % self.foaf_ns)
        publisher_name.text = row['publisher']

        #Add ContactPoint
        for contactPoint in row["contactPoints"]:
            dataset_contactPoint = etree.SubElement(dataset_element, "{%s}contactPoint" % self.dcat_ns )
            vcard_org = etree.SubElement(dataset_contactPoint, "{%s}Organization" % self.vcard_ns)
            vcard_fn = etree.SubElement(vcard_org, "{%s}fn" % self.vcard_ns)
            vcard_fn.text = contactPoint["contactPointName"]
            if not pd.isna(contactPoint['contactPointMail']):
                vcard_hasEmail = etree.SubElement(vcard_org, "{%s}hasEmail" % self.vcard_ns, attrib={"{%s}resource" % self.rdf_ns: f"mailto:{contactPoint['contactPointMail']}"})

        # Add license
        license_element = etree.SubElement(dataset_element, "{%s}license" % self.dct_ns, attrib={"{%s}resource" % self.rdf_ns:"http://dcat-ap.ch/vocabulary/licenses/terms_open"})

        # Add distributions for different formats
        
        for file in row["files"]:
            distribution = etree.SubElement(dataset_element, "{%s}distribution" % self.dcat_ns)
            distribution_element = etree.SubElement(distribution, "{%s}Distribution" % self.dcat_ns)
            distribution_title = etree.SubElement(distribution_element, "{%s}title" % self.dct_ns, attrib={"{%s}lang" % self.xml_ns: f"de"})
            distribution_title.text = file["key"]
            description = etree.SubElement(distribution_element, "{%s}description" % self.dct_ns, attrib={"{%s}lang" % self.xml_ns: f"de"})
            description.text = row['description']
            access_url = etree.SubElement(distribution_element, "{%s}accessURL" % self.dcat_ns, attrib={"{%s}resource" % self.rdf_ns: file["links"]["content"]})
            downloadUrl = etree.SubElement(distribution_element, "{%s}downloadURL" % self.dcat_ns, attrib={"{%s}resource" % self.rdf_ns: file["links"]["content"]})
            format_element = etree.SubElement(distribution_element, "{%s}format" % self.dct_ns, attrib={"{%s}resource" % self.rdf_ns: self.fileTypes[file["mimetype"]]})
            distribution_rights = etree.SubElement(distribution_element,"{%s}rights" % self.dct_ns)
            distribution_rights.text = "http://creativecommons.org/publicdomain/zero/1.0/"
            license_element = etree.SubElement(distribution_element, "{%s}license" % self.dct_ns, attrib={"{%s}resource" % self.rdf_ns:"http://dcat-ap.ch/vocabulary/licenses/terms_open"})
            distribution_byteSize = etree.SubElement(distribution_element, "{%s}byteSize" % self.dcat_ns, attrib={"{%s}datatype" % self.rdf_ns:"http://www.w3.org/2001/XMLSchema#decimal"})
            distribution_byteSize.text = str(file["size"])
            distribution_issued = etree.SubElement(distribution_element, "{%s}issued" % self.dct_ns, attrib={"{%s}datatype" % self.rdf_ns: "http://www.w3.org/2001/XMLSchema#dateTime"})
            distribution_issued.text = self.getDCATDateTime(file['created'])
            distribution_modified = etree.SubElement(distribution_element, "{%s}modified" % self.dct_ns, attrib={"{%s}datatype" % self.rdf_ns: "http://www.w3.org/2001/XMLSchema#dateTime"})
            distribution_modified.text = self.getDCATDateTime(file['updated'])
        
        return self.root, self.catalog

if __name__ == "__main__":
    dcat = DCAT()
    dcat.createCatalog(catalogUri="https://sandbox.zenodo.org/communities/lara_zentralgut_datasets", catalogTitle="ZentralGut.ch Datasets", catalogDesc="Metadata records of ressources available in Central Swiss cultural heritage repository ZentralGut.ch", catalogPublisher="Zentral- und Hochschulbibliothek Luzern")

    sample_row = {
        'ZenodoId':'103436',
        'title': 'Gemälde der Hofbrücke zu Luzern (generated XML)',
        'description': 'Auf ZentralGut.ch finden Sie digitale Fotografien der Brückengemälde der Hofbrücke zu Luzern aus dem Bestand des Stadtarchivs Luzern',
        "created": "2024-08-23T19:35:33.026310+00:00",
        "modified": "2024-08-23T19:35:33.285038+00:00",
        'publication intervall': 'http://publications.europa.eu/resource/authority/frequency/AS_NEEDED',
        'publisher': 'Zentral- und Hochschulbibliothek Luzern (ZHB Luzern)',
        'contactPoints': [
            {
                'contactPointName' : 'ZentralGut',
                'contactPointMail' : 'zentralgut@zhbluzern.ch'
            },
            {
                'contactPointName' : 'Stadtarchiv Luzern',
                'contactPointMail' : 'stadtarchiv@stadtluzern.ch'
            }  
        ],
        'files': [
            {
                "key" : "stadtarchivluzern_hofbruecke.csv",
                'links': {'content': 'https://sandbox.zenodo.org/api/records/103437/files/stadtarchivluzern_hofbruecke.csv/content'},
                'size':"143339",
                "mimetype":"text/csv",
                "created": "2024-08-23T19:35:33.026310+00:00",
                "modified": "2024-08-23T19:35:33.285038+00:00"
            },
            {
                "key" : "stadtarchivluzern_hofbruecke.json",
                'links': {'content': 'https://sandbox.zenodo.org/api/records/103437/files/stadtarchivluzern_hofbruecke.json/content'},
                'size':"283135",
                "mimetype":"application/json",
                "created": "2024-08-23T19:35:33.026310+00:00",
                "modified": "2024-08-23T19:35:33.285038+00:00"                },
            {
                "key" : "stadtarchivluzern_hofbruecke.xml",
                'links': {'content': 'https://sandbox.zenodo.org/api/records/103437/files/stadtarchivluzern_hofbruecke.xml/content'},
                'size':"1763163",
                "mimetype":"text/xml",
                "created": "2024-08-23T19:35:33.026310+00:00",
                "modified": "2024-08-23T19:35:33.285038+00:00"
            }
        ]

    }
    dcatXml = dcat.createDataset(sample_row)

    xml_str = etree.tostring(dcatXml[0], pretty_print=True, xml_declaration=True, encoding='UTF-8')
    with open("Files/ZentralGut_DCAT.xml", "wb") as xml_file:
        xml_file.write(xml_str)
    print("DCAT-XML file has been generated successfully.")