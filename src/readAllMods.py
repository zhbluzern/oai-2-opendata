from lxml import etree
import json
import pandas as pd

def buildKey(node, typeAttr):
    nodeName = etree.QName(node).localname 
    if typeAttr != None:
        nodeName = f"{nodeName}_{typeAttr}"
    return nodeName

def getNodeValue(resultDet, node, parentType=None):
    typeAttr = node.get("type")
    if typeAttr == None and parentType != None:
        typeAttr = parentType
    #print(typeAttr)
    resultDet[buildKey(node, typeAttr)] = node.text
    return resultDet

def process_FirstMods(resultDet, node, parentTypeAttr=None):
    # Check if the node has no children (base case)
    if len(node.getchildren()) == 0:
        return getNodeValue(resultDet, node, parentTypeAttr)
    else:
        if parentTypeAttr == None:
            parentTypeAttr = node.get("type")
        # If the node has children, recurse on each child
        for child in node.getchildren():
            resultDet = process_FirstMods(resultDet, child, parentTypeAttr)
    return resultDet

def process_Metadata(xmlData, namespaces):
    resultDet = {}
    mods = xmlData.xpath(".//mods:mods[1]", namespaces=namespaces)
    # Run through First-Mods
    for node in mods[0].getchildren():
        resultDet = process_FirstMods(resultDet, node)

    # Get DocType
    docType = xmlData.xpath(".//mets:structMap[@TYPE='LOGICAL'][1]/mets:div", namespaces=namespaces)
    resultDet["documentType"] = docType[0].get("TYPE")

    # Extract File-URLs
    fileUrls = xmlData.xpath(".//mets:fileGrp[@USE='DEFAULT']/mets:file", namespaces=namespaces)
    resultDet["fileUrl"] = []
    for fileUrlNode in fileUrls:
        fileUrl = fileUrlNode.xpath(".//mets:FLocat/@xlink:href", namespaces=namespaces)
        resultDet["fileUrl"].append({fileUrlNode.get("ID") : fileUrl[0]})

    # Extract IIIF-URLs
    fileUrls = xmlData.xpath(".//mets:fileGrp[@USE='IIIF']/mets:file", namespaces=namespaces)
    resultDet["iiifUrl"] = []
    for fileUrlNode in fileUrls:
        fileUrl = fileUrlNode.xpath(".//mets:FLocat/@xlink:href", namespaces=namespaces)
        resultDet["iiifUrl"].append({fileUrlNode.get("ID") : fileUrl[0]})
    if resultDet["iiifUrl"] == []:
        del resultDet["iiifUrl"]

    # Return Object
    return resultDet


if __name__ == "__main__":
    root = etree.parse('Files/test.xml').getroot()
    namespaces = { "xmlns" : "http://www.openarchives.org/OAI/2.0/", "mods":"http://www.loc.gov/mods/v3", "mets" : "http://www.loc.gov/METS/", "xlink":"http://www.w3.org/1999/xlink"}
    resultDet = process_Metadata(root, namespaces)
    print(json.dumps(resultDet, indent=2))
    df = pd.DataFrame(resultDet) 
    df.to_csv('Files/test.csv', index=False)