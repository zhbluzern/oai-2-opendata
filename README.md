# OAI-PMH 2 OpenData

Diese Skriptsammlung ermöglicht das Harvesten von OAI-PMH Schnittstellen, Transformation der Daten in "einfache" Formate, Deponierung der Datenfiles auf Zenodo und Erzeugung einer DCAT konformen Metadatendatei zur Registrierung der offenen Daten in CKAN-Plattformen.

* Komplettes OAI-PMH-XML in einem File
* Konvertierung ausgewählter Metadaten in ein CSV und ein JSON
* Upload der Metadatenfiles auf Zenodo
* Erstellung eines DCAT-XML-Files für "Katalogisierung" in Open Data-Plattformen.

## Metadaten-Konvertierung

Das vorliegende Skript geht davon aus, dass das ein METS/MODS-Format von der OAI-PMH-Schnittstelle ausgeliefert wird. Der gesamte erste `mods:mods`-Block wird zur Gänze ausgelesen und für den csv/json-Export bereitgestellt. Zusätzlich werden noch file-Urls und iiif-Urls in die Exportformate übernommen. 

## Hinweis

Dieses Skript wurde erstellt um einen niederschwelligen Zugang zu den offenen Metadaten der ZHB Luzern zu ermöglichen.