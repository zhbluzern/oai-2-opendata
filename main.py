import src.createDataset as createDataset
import src.invenioRest as invenioRest
import src.ZenodoRest as zenodoRest
import src.handleInvenio as handleInvenio
import src.handleZenodo as handleZenodo
import src.createDCAT as createDCAT
import pandas as pd
from lxml import etree

# Create different datasets based on OAI-PMH for upload 2 opendataSwiss platform
dcat = createDCAT.DCAT()
dcat.createCatalog(catalogUri="https://sandbox.zenodo.org/communities/lara_zentralgut_datasets", catalogTitle="ZentralGut.ch Datasets", catalogDesc="Metadata records of ressources available in Central Swiss cultural heritage repository ZentralGut.ch", catalogPublisher="Zentral- und Hochschulbibliothek Luzern")

# Basic Configuration - list all OAI-Sets with necessary Metadata in a separate XLS-File as control file for the whole process here
inputFileName = "config.xlsx"
data = pd.read_excel(inputFileName, dtype=str)
df = pd.DataFrame(data)

for index, row in df.iterrows():
    ## Define necessary parameters to harvest  OAI-PMH
    oaiUrl = "https://zentralgut.ch/oai"
    #params = {"verb":"ListRecords", "metadataPrefix":"mets", "set": "DC:zentralundhochschulbibliothekluzern.portraitgalerie"}
    oaiParams = {"verb":"ListRecords", "metadataPrefix":"mets", "set": row.oaiSet}

    ## Create the Dateset files (csv, json, xml)
    createDataset.createDataset(oaiUrl, oaiParams, row.outputFileName)

    ## Upload the Dateset files to Zenodo
    ### Create a new Zenodo-Object
    #zenodo = invenioRest.Invenio()
    #zenodo.resetRecord()

    if pd.isna(row.ZenodoId):
        print("create a new zenodo Record")
        zenodo = zenodoRest.Zenodo()
    else:
        print(f"update Zenodo Record {row.ZenodoId}")
        zenodo = zenodoRest.Zenodo(row.ZenodoId)
 
    
    ### Create the Draft with all given Metadata    
    #newRecord = handleInvenio.prepareDraft(zenodo, row)
    metadata = handleZenodo.createZenodoMetadata(row)
    print(zenodo.putRecordData(metadata).text)

    ### Upload the Dataset Files
    #handleInvenio.uploadFiles(zenodo, row, newRecord)
    print(handleZenodo.uploadFiles(zenodo, row))

    ### Publish the Zenodo Record
    #print(zenodo.publishDraft(newRecord["id"]))
    print(zenodo.publishRecord())
    
    ### Accept open Requests for this new record (e.g. Community-Adding)
    #print(handleInvenio.acceptAllRequests(zenodo, newRecord))
    zenRequ = zenodo.searchRequests(f"topic.record:{zenodo.ZenodoId}")
    results = (zenRequ.json())
    #Es dürfen nur soviele Requests wie Communities vorliegen, sonst ist was faul.
    if (len(results["hits"]["hits"])) == 1:
        for hit in results["hits"]["hits"]:
            print(zenodo.acceptRequest(hit["id"]).text)
    
    #Write Zenodo-ID to XLS
    df.at[index, 'ZenodoId'] = str(zenodo.ZenodoId)
    row["ZenodoId"] = str(zenodo.ZenodoId)
    df.to_excel(inputFileName, index=False, engine="openpyxl")
    #break

    #Prepare DCAT-Metadatafile by given data for current row
    dcat_row = dcat.prepareData(row)
    dcatXml = dcat.createDataset(dcat_row)

#Export DCAT-XML
xml_str = etree.tostring(dcatXml[0], pretty_print=True, xml_declaration=True, encoding='UTF-8')
with open("Files/ZentralGut_DCAT.xml", "wb") as xml_file:
    xml_file.write(xml_str)
print("DCAT-XML file has been generated successfully.")